# README #

Shell client to interact with any project that provide a sqlite-bridge instance running.

### What is this repository for? ###

* Aim to help to developers to query your database running into an android app.

### How to execute?###
* Download the code an export a jar file name sqlite-bridge-client.jar
* Run jar from above with

```
#!java
 java -Duser.home=$HOME -cp sqlite-bridge-client.jar:libs/*:. com.github.sqlitebridge.client.SqliteBridgeClient

```
**IMPORTANT** you must provide the libraries listed on *libs/* folder in your classpath like above.

You should get the prompt line character 


```
#!java
$
```
 * Type in *help* command to see available list of commands.

```
#!java
$ help
+------------+---------------------------------------------------------------------------------------------+
|   Command  |                                         Description                                         |
+------------+---------------------------------------------------------------------------------------------+
| disconnect | Disconnect from server.                                                                     |
|       exit | Exit from the app also disconnect current socket channel.                                   |
|       help | List of available commands.                                                                 |
|    connect | Connect to an specific server based on URL and PORT argument. Retry 3 times before to fail. |
+------------+---------------------------------------------------------------------------------------------+

Ok
```


### How to use it?###
If you run the android example project in your phone you would be able to query the database into the phone. 

* Phone's IP 192.168.1.102
* Run Sqlite-bridge-example in your phone (in separated repo [here](https://bitbucket.org/vsmartinez/sqlite-bridge-example))
* First of all you must connect to the sqlite-bridge instance in your phone

```
#!java
$ connect 192.168.1.102 20004
```

* Once you got connected prompt line will display the IP number of your phone. Three times it will retry to connect before to fail.

```
#!java
Connecting to 192.168.1.102:20004
Client ready...
Ok
192.168.1.102$ 
```

* Run a *select* statement to query your database, due is a brand new database you will get an empty result set. 

```
#!java
192.168.1.102$ select * from tblUser
Ok
+--------+----------+------+
| userID | userName | name |
+--------+----------+------+
|        |          |      |
+--------+----------+------+

192.168.1.102$ 
```

* Perform some *insert* statements to add some values to the database
```
#!java
192.168.1.102$ insert into tblUser values (1, "vsmartinez", "Vidal Santiago Martinez")
Ok
192.168.1.102$ insert into tblUser values (2, "jdoe", "John Doe")
Ok
192.168.1.102
```
* Once again a *select* statement

```
#!java
192.168.1.102$ select * from tblUser
Ok
+--------+------------+-------------------------+
| userID |  userName  |           name          |
+--------+------------+-------------------------+
|      1 | vsmartinez | Vidal Santiago Martinez |
|      2 |       jdoe |                John Doe |
+--------+------------+-------------------------+


192.168.1.102$ 
```

* You can run any query as you want, *delete* is also allowed.
* You can do even more complex statements or request for your database pragma as follow

```
#!java
192.168.1.102$ pragma table_info(tbluser)
Ok
+-----+----------+---------+---------+------------+----+
| cid |   name   |   type  | notnull | dflt_value | pk |
+-----+----------+---------+---------+------------+----+
|   0 |   userID | INTEGER |       1 |       null |  1 |
|   1 | userName |    TEXT |       1 |       null |  0 |
|   2 |     name |    TEXT |       0 |       null |  0 |
+-----+----------+---------+---------+------------+----+

192.168.1.102$ 

```
* Explain your query plan
```
#!java

192.168.1.102$ explain query plan select count(1) from tblUser
Ok
+----------+-------+------+--------------------------------------------------------------------+
| selectid | order | from |                               detail                               |
+----------+-------+------+--------------------------------------------------------------------+
|        0 |     0 |    0 | SCAN TABLE tblUser USING COVERING INDEX sqlite_autoindex_tblUser_1 |
+----------+-------+------+--------------------------------------------------------------------+

192.168.1.102$ 
```

* To *disconnect* from your remote database just run

```
#!java

192.168.1.102$ disconnect
Ok
$ 
```

and you will get your default prompt character


* To exit from this client just run *exit* command and you will get your OS command line.

### Tips ###
* Command line browsing is allowed.
* **ctrl+r** allow you to search in your command history, just like linux shell does.

# License #
```
Copyright 2015 Vidal Santiago Martinez. vidalsantiagomartinez at gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
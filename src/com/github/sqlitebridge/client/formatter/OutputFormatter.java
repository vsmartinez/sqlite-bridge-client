package com.github.sqlitebridge.client.formatter;

import java.io.PrintWriter;

import com.github.sqlitebridge.client.statement.Statement.Result;

public interface OutputFormatter {

	void print(Result result, PrintWriter out);

	void print(Result result, PrintWriter out, PrintWriter err);

}

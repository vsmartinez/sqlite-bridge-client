package com.github.sqlitebridge.client.formatter;

import java.io.PrintWriter;

import com.bethecoder.ascii_table.ASCIITable;
import com.github.sqlitebridge.client.statement.Statement.Result;

public class TableFormatterConsole implements OutputFormatter {

	@Override
	public void print(Result result, PrintWriter out) {
		print(result, out, out);
	}

	@Override
	public void print(Result result, PrintWriter out, PrintWriter err) {
		if (!result.isResult()) {
			out.println("Error");
			if (result.getMessage() != null) {
				out.println(result.getMessage());
			}
			return;
		}

		out.println("Ok");

		if (result.getHeaders() != null && result.getData() != null) {
			ASCIITable.getInstance().printTable(result.getHeaders(),
					result.getData());
		}
	}

}

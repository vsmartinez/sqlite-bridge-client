package com.github.sqlitebridge.client.command;

public class CommandException extends Exception {

	private static final long serialVersionUID = -6965821480735524826L;

	public CommandException(String msg) {
		super(msg);
	}

}

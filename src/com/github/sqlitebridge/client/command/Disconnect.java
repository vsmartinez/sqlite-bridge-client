package com.github.sqlitebridge.client.command;

import java.io.IOException;

import com.github.sqlitebridge.client.ConnectionManager;
import com.github.sqlitebridge.client.cli.CliConsole;

public class Disconnect implements Command {

	private ConnectionManager connectionManager;

	public Disconnect(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public String getName() {
		return "disconnect";
	}

	@Override
	public boolean execute(CliConsole cli, String... args)
			throws CommandException {
		return execute(cli);
	}

	@Override
	public boolean execute(CliConsole cli) throws CommandException {
		try {
			connectionManager.disconnect();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return true;
		}
	}

	@Override
	public String getDescription() {
		return "Disconnect from server.";
	}

}

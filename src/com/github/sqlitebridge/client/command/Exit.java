package com.github.sqlitebridge.client.command;

import com.github.sqlitebridge.client.SqliteBridgeClient;
import com.github.sqlitebridge.client.cli.CliConsole;

public class Exit implements Command {

	private SqliteBridgeClient client;

	public Exit(SqliteBridgeClient client) {
		this.client = client;
	}

	@Override
	public String getName() {
		return "exit";
	}

	@Override
	public boolean execute(CliConsole cli, String... args)
			throws CommandException {
		return execute(cli);
	}

	@Override
	public boolean execute(CliConsole cli) throws CommandException {
		client.stop();
		return true;
	}

	@Override
	public String getDescription() {
		return "Exit from the app also disconnect current socket channel.";
	}

}

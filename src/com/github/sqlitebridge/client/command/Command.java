package com.github.sqlitebridge.client.command;

import com.github.sqlitebridge.client.cli.CliConsole;

public interface Command {

	String getName();

	String getDescription();

	boolean execute(CliConsole cli, String... args) throws CommandException;

	boolean execute(CliConsole cli) throws CommandException;
}

package com.github.sqlitebridge.client.command;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import com.github.sqlitebridge.client.ConnectionManager;
import com.github.sqlitebridge.client.cli.CliConsole;

public class Connect implements Command {

	private static final long WAIT = 2000; // 2000 millis
	private static int MAX_TRY = 3;
	private ConnectionManager connectionManager;

	public Connect(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public boolean execute(CliConsole cli, String... args) throws CommandException {
		if (connectionManager.isConnected()) {
			System.out.println(String.format("Already connected to %s",
					connectionManager.getHost()));
			return true;
		}
		
		if (args.length != 3) {
			throw new CommandException("Two argument expected <URL> <PORT>");
		}

		int retry = 0;
		while (retry < MAX_TRY) {
			Socket sc = null;
			try {
				System.out.println(String.format("Connecting to %s:%s",
						args[1], args[2]));

				connectionManager.connect(args[1], Integer.valueOf(args[2]));

				System.out.println("Client ready...");

				return true;
			} catch (UnknownHostException e) {
				retry = retry + 1;
				System.err.println(String.format(
						"Unknow host, waiting about %.0f sec", WAIT / 1000f));
				sleep(WAIT);
				System.out.println("retry " + retry);

			} catch (IOException e) {
				retry = retry + 1;
				System.err.println(String.format(
						"IO Exception, waiting about %.0f sec", WAIT / 1000f));
				sleep(WAIT);
				System.out.println("retry " + retry);
			} finally {
				if (sc != null) {
					try {
						sc.close();
					} catch (IOException e) {
					}
				}
			}
		}
		return false;
	}

	private void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			// Nothing to do
		}
	}

	@Override
	public boolean execute(CliConsole cli) throws CommandException {
		throw new CommandException("Two argument expected <URL> <PORT>");
	}

	@Override
	public String getName() {
		return "connect";
	}

	@Override
	public String getDescription() {
		return "Connect to an specific server based on URL and PORT argument. Retry 3 times before to fail.";
	}

}

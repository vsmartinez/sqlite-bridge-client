package com.github.sqlitebridge.client.command;

import java.util.Map;

import com.bethecoder.ascii_table.ASCIITable;
import com.bethecoder.ascii_table.ASCIITableHeader;
import com.github.sqlitebridge.client.cli.CliConsole;

public class Help implements Command {

	private Map<String, Command> commands;

	public Help(Map<String, Command> commands) {
		this.commands = commands;
	}

	@Override
	public String getName() {
		return "help";
	}

	@Override
	public boolean execute(CliConsole cli, String... args)
			throws CommandException {
		return execute(cli);
	}

	@Override
	public boolean execute(CliConsole cli) throws CommandException {

		String[][] data = new String[commands.size()][2];

		int i = 0;
		for (Map.Entry<String, Command> entry : commands.entrySet()) {
			data[i][0] = entry.getValue().getName();
			data[i][1] = entry.getValue().getDescription();
			i++;
		}

		ASCIITableHeader[] headers = {
				new ASCIITableHeader("Command", ASCIITable.ALIGN_RIGHT),
				new ASCIITableHeader("Description", ASCIITable.ALIGN_LEFT) };
		ASCIITable.getInstance().printTable(headers, data);

		return true;
	}

	@Override
	public String getDescription() {
		return "List of available commands.";
	}

}

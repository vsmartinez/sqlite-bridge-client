package com.github.sqlitebridge.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.github.sqlitebridge.client.cli.CliConsole;
import com.github.sqlitebridge.client.command.Command;
import com.github.sqlitebridge.client.command.CommandException;
import com.github.sqlitebridge.client.command.Connect;
import com.github.sqlitebridge.client.command.Disconnect;
import com.github.sqlitebridge.client.command.Exit;
import com.github.sqlitebridge.client.command.Help;
import com.github.sqlitebridge.client.formatter.OutputFormatter;
import com.github.sqlitebridge.client.formatter.TableFormatterConsole;
import com.github.sqlitebridge.client.statement.QueryStatement;
import com.github.sqlitebridge.client.statement.RawStatement;
import com.github.sqlitebridge.client.statement.Statement;
import com.github.sqlitebridge.client.statement.Statement.Result;

public class SqliteBridgeClient {

	private static final String SELECT_STATEMENT = "select";
	private static final String INSERT_STATEMENT = "insert";
	private static final String UPDATE_STATEMENT = "update";
	private static final String DELETE_STATEMENT = "delete";

	private Map<String, Statement> statements = new HashMap<String, Statement>();

	private Map<String, Command> commands = new HashMap<String, Command>();

	private volatile boolean stop = false;

	private OutputFormatter formatter = null;

	private ConnectionManager connectionManager = null;
	private String PROMPT = "$ ";

	public SqliteBridgeClient(OutputFormatter formatter) {
		if (formatter == null) {
			throw new IllegalArgumentException("formatter can't be null.");
		}

		// Output formatter
		this.formatter = formatter;

		// Default Statement
		addStatement(new QueryStatement(SELECT_STATEMENT));
		addStatement(new RawStatement(INSERT_STATEMENT));
		addStatement(new RawStatement(UPDATE_STATEMENT));
		addStatement(new RawStatement(DELETE_STATEMENT));

		connectionManager = new ConnectionManager();

		// Commands available
		addCommand(new Connect(connectionManager));
		addCommand(new Disconnect(connectionManager));
		addCommand(new Exit(this));
		addCommand(new Help(commands));

	}

	public void stop() {
		try {
			connectionManager.disconnect();
		} catch (IOException e) {
			// Nothing to do, just stop the loop
		}
		stop = true;
	}

	private String buildPrompt() {
		if (connectionManager.isConnected()) {
			return connectionManager.getHost() + PROMPT;
		}

		return PROMPT;
	}

	public void start() {
		CliConsole cli = new CliConsole();
		while (!stop) {
			try {

				stop = false;

				String str = cli.readLine(buildPrompt());
				if (str == null || str.trim().length() == 0) {
					continue;
				}

				// TODO improve this
				String[] inputSplit = str.trim()
						.toLowerCase(Locale.getDefault()).split(" ");
				String tmp = inputSplit[0];
				
				try {
					if (processCommand(inputSplit, cli)) {
						cli.println("Ok");
						continue;
					}
				} catch (CommandException e) {
					cli.println(e.getMessage());
					continue;
				}

				// Only send to the server if is connected
				if (!connectionManager.isConnected()) {
					cli.println("Not connected to the server.");
					continue;
				}

				Statement statement = statements.get(tmp);
				if (statement == null) {
					// System.out.println("No command defined for " + tmp
					// + ", running with default one.");
					statement = new QueryStatement("");
				}

				Result result = statement.execute(str, connectionManager);

				formatter.print(result, cli.getWriter());

			} catch (IOException e) {
				cli.println(e.getMessage());
			}
		}

		cli.close();
	}

	private boolean processCommand(String[] input, CliConsole cli) throws CommandException {
		String cmd = input[0];
		Command command = commands.get(cmd);
		if (command == null) {
			return false;
		}
		return command.execute(cli, input);
	}

	public void addStatement(Statement statement) {
		if (statement == null) {
			return;
		}
		statements.put(statement.getName(), statement);
	}

	public void removeStatement(Statement statement) {
		if (statement == null) {
			return;
		}
		statements.remove(statement.getName());
	}

	public void removeStatement(String name) {
		if (name == null) {
			return;
		}
		statements.remove(name);
	}

	public void addCommand(Command command) {
		commands.put(command.getName(), command);
	}

	public void removeCommand(Command command) {
		commands.remove(command.getName());
	}

	public void removeCommand(String name) {
		if (name != null) {
			return;
		}

		commands.remove(name);
	}

	public static void main(String[] args) throws IOException {
		SqliteBridgeClient client = new SqliteBridgeClient(
				new TableFormatterConsole());
		client.start();
	}

}

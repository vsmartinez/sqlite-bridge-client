package com.github.sqlitebridge.client.statement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class QueryStatement extends Statement {

	private String name;

	public QueryStatement(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Result read(Reader reader) throws IOException {

		BufferedReader br = new BufferedReader(reader);
		boolean result = readResult(br);

		if (!result) {
			String msg = readError(br);
			return new Result(result, msg, null, null);
		} else {
			String[] headers = readHeader(br);

			int ch = -1;
			StringBuffer sb = new StringBuffer();
			List<String[]> rows = new ArrayList<String[]>();
			while ((ch = br.read()) != -1) {
				if (ch == 65535) {
					// process message
					String[] answer = sb.toString().split("\n");
					for (String str : answer) {
						String[] line = str.split(",");
						rows.add(line);
					}
					sb.setLength(0);
					break;
				} else {
					sb.append((char) ch);
				}
			}

			String[][] dataset = rows
					.toArray(new String[rows.size() - 1][headers.length - 1]);

			return new Result(result, null, headers, dataset);
		}
	}
}

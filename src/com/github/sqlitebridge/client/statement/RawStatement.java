package com.github.sqlitebridge.client.statement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class RawStatement extends Statement {

	private String name;

	public RawStatement(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Result read(Reader reader) throws IOException {
		BufferedReader br = new BufferedReader(reader);
		boolean result = readResult(br);
		if (!result) {
			String msg = readError(br);
			return new Result(result, msg, null, null);
		}

		return new Result(result, null, null, null);
	}

}

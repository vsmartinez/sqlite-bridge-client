package com.github.sqlitebridge.client.statement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import com.github.sqlitebridge.client.ConnectionManager;

public abstract class Statement {
	public static final String OK = "ok";

	public abstract String getName();

	protected abstract Result read(Reader reader) throws IOException;

	public Result execute(String str, ConnectionManager connectionManager) throws IOException {
		Writer writer = connectionManager.getWriter();
		writer.write(str);
		writer.write(-1);
		writer.flush();
		
		return read(connectionManager.getReader());
	};

	protected static boolean readResult(BufferedReader reader)
			throws IOException {
		String result = reader.readLine();
		return result != null && OK.equalsIgnoreCase(result);
	}

	protected static String[] readHeader(BufferedReader reader)
			throws IOException {
		String str = reader.readLine();
		return str.trim().split(",");
	}

	protected static String readError(BufferedReader reader) throws IOException {
		return reader.readLine();
	}

	public static class Result {
		private boolean result;
		private String msg;
		private String[] headers;
		private String[][] data;

		public Result(boolean result, String msg, String[] headers,
				String[][] data) {
			this.setResult(result);
			this.msg = msg;
			this.headers = headers;
			this.data = data;
		}

		public String getMessage() {
			return msg;
		}

		public void setMessage(String msg) {
			this.msg = msg;
		}

		public String[] getHeaders() {
			return headers;
		}

		public void setHeaders(String[] headers) {
			this.headers = headers;
		}

		public String[][] getData() {
			return data;
		}

		public void setData(String[][] data) {
			this.data = data;
		}

		public boolean isResult() {
			return result;
		}

		public void setResult(boolean result) {
			this.result = result;
		}
	}
}

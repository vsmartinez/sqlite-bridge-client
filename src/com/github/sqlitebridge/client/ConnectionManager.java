package com.github.sqlitebridge.client;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import org.omg.CORBA.TIMEOUT;

public class ConnectionManager {

	private static final int TIMEOUT = 5000;
	private Socket socket;
	private Writer writer;
	private Reader reader;

	private volatile boolean connected = false;
	private String host;
	private Integer port;

	public void connect(String host, Integer port) throws UnknownHostException,
			IOException {
		this.host = host;
		this.port = port;
		socket = new Socket();
		socket.connect(new InetSocketAddress(host, port), TIMEOUT);
		socket.setSoTimeout(TIMEOUT);
		socket.setKeepAlive(true);

		writer = new PrintWriter(socket.getOutputStream());
		reader = new InputStreamReader(socket.getInputStream());

		connected = true;
	}

	public void disconnect() throws IOException {
		if (!connected) {
			return;
		}

		connected = false;

		writer.write(".disconnect");
		writer.write('\n');
		writer.write(-1);
		writer.flush();
		socket.close();
	}

	public boolean isConnected() {
		return connected;
	}

	public Writer getWriter() {
		return writer;
	}

	public Reader getReader() {
		return reader;
	}

	public String getHost() {
		return host;
	}

}
